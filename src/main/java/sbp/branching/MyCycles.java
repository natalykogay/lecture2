package sbp.branching;

import sbp.common.Utils;

public class MyCycles {
    private final Utils utils;

    public MyCycles(Utils utils) {
        this.utils = utils;
    }

    /**
     * Необходимо написать реализацию метода с использованием for()
     * +     * Метод должен вызвать Utils#utilFunc1() на каждой итерации
     * +     * Реализация Utils#utilFunc1() неизвестна
     * +     * Должна присутствовать проверка возврщаемого значения от Utils#utilFunc1()
     * Результат проверяется через unit-test (все тесты должны выполниться успешно)
     *
     * @param iterations - количество итераций
     * @param str        - строка для вывода через утилиту {@link Utils}
     */
    public void cycleForExample(int iterations, String str) {
        boolean result = true;
        for (int i = 0; i < iterations; i++) {
            if (result) {
                result = utils.utilFunc1(str);
            }
        }
    }

    /**
     * Необходимо написать реализацию метода с использованием while()
     * +     * Метод должен вызвать Utils#utilFunc1() на каждой итерации
     * +     * Реализация Utils#utilFunc1() неизвестна
     * +     * Должна присутствовать проверка возврщаемого значения от Utils#utilFunc1()
     * Результат проверяется через unit-test (все тесты должны выполниться успешно)
     *
     * @param iterations - количество итераций
     * @param str        - строка для вывода через утилиту {@link Utils}
     */
    public void cycleWhileExample(int iterations, String str) {
        boolean result = true;
        while (iterations > 0) {
            if (result) {
                result = utils.utilFunc1(str);
            }
            iterations--;
        }
    }

    /**
     * Необходимо написать реализацию метода с использованием while()
     * -     * Метод должен вызвать Utils#utilFunc1() на каждой итерации
     * -     * Реализация Utils#utilFunc1() неизвестна
     * +     * Метод должен вызвать Utils#utilFunc1() на каждой итерации
     * +     * Реализация Utils#utilFunc1() неизвестна
     * Должна присутствовать проверка возврщаемого значения
     * Результат проверяется через unit-test (все тесты должны выполниться успешно)
     *
     * @param from - начальное значение итератора
     * @param to   - конечное значение итератора
     * @param str  - строка для вывода через утилиту {@link Utils}
     */
    public void cycleDoWhileExample(int from, int to, String str) {
        do {
            boolean result = utils.utilFunc1(str);
            if (result) {
                from--;
            } else {
                from++;
            }
        } while (from <= to);
    }
}