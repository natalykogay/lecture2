package sbp.branching;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import sbp.common.Utils;


/**
 * Класс с юнит тестами для методов класса {@link MyBranching}
 */
public class MyBranchingTest {
    /**
     * Выполнение метода maxInt() при utils.utilFunc2() = true и utils.utilFunc1 = false;
     */
    @Test
    public void maxIntUtilFunc1FalseTest() {
        int compareValue = 0;
        Utils utils = Mockito.mock(Utils.class);
        Mockito.when(utils.utilFunc2()).thenReturn(true);
        Mockito.when(utils.utilFunc1(Mockito.anyString())).thenReturn(false);
        MyBranching myBranching = new MyBranching(utils);
        int result = myBranching.maxInt(3, 6);

        Assertions.assertEquals(compareValue, result, "result не равен ожидаемому значению - " + compareValue);
        Mockito.verify(utils, Mockito.times(1)).utilFunc2();
        Mockito.verify(utils, Mockito.times(6)).utilFunc1(Mockito.anyString());
    }
    /**
     * Выполнение метода maxInt() при utils.utilFunc2() = true и utils.utilFunc1 = true;
     */
    @Test
    public void maxIntUtilTrueTest() {
        int compareValue = 0;
        Utils utils = Mockito.mock(Utils.class);
        Mockito.when(utils.utilFunc2()).thenReturn(true);
        Mockito.when(utils.utilFunc1(Mockito.anyString())).thenReturn(true);
        MyBranching myBranching = new MyBranching(utils);
        int result = myBranching.maxInt(3, 6);

        Assertions.assertEquals(compareValue, result, "result не равен ожидаемому значению - " + compareValue);
        Mockito.verify(utils, Mockito.times(1)).utilFunc2();
        Mockito.verify(utils, Mockito.times(1)).utilFunc1(Mockito.anyString());
    }

    /**
     * Выполнение метода maxInt() при utils.utilFunc2() = false;
     */
    @Test
    public void maxIntUtilFunc2FalseTest() {
        int value1 = 3;
        int value2 = 6;
        Utils utils = Mockito.mock(Utils.class);
        Mockito.when(utils.utilFunc2()).thenReturn(false);
        Mockito.when(utils.utilFunc1(Mockito.anyString())).thenReturn(true);
        MyBranching myBranching = new MyBranching(utils);
        int result = myBranching.maxInt(value1, value2);

        Assertions.assertEquals(value2, result, "result не равен ожидаемому значению - " + value2);
        Mockito.verify(utils, Mockito.times(1)).utilFunc2();
        Mockito.verify(utils, Mockito.never()).utilFunc1(Mockito.anyString());
    }

    /**
     * Успешное выполнение метода ifElseExample();
     */
    @Test
    public void ifElseExampleSuccessTest() {
        Utils utils = Mockito.mock(Utils.class);
        Mockito.when(utils.utilFunc2()).thenReturn(true);
        MyBranching myBranching = new MyBranching(utils);
        boolean result = myBranching.ifElseExample();

        Assertions.assertTrue(result);
        Mockito.verify(utils, Mockito.times(1)).utilFunc2();
        Mockito.verify(utils, Mockito.never()).utilFunc1(Mockito.anyString());
    }

    /**
     * Неуспешное выполнение метода ifElseExample();
     */
    @Test
    public void ifElseExampleNegativeTest() {
        Utils utils = Mockito.mock(Utils.class);
        Mockito.when(utils.utilFunc2()).thenReturn(false);
        MyBranching myBranching = new MyBranching(utils);
        boolean result = myBranching.ifElseExample();

        Assertions.assertFalse(result);
        Mockito.verify(utils, Mockito.times(1)).utilFunc2();
        Mockito.verify(utils, Mockito.never()).utilFunc1(Mockito.anyString());
    }
    /**
     * Проверка выполнения Utils#utilFunc1 = false и Utils#utilFunc2 = true при входящем i = 0
     */
    @Test
    public void switchExampleValue0Func2TrueTest() {
        int i = 0;
        Utils utils = Mockito.mock(Utils.class);
        Mockito.when(utils.utilFunc2()).thenReturn(true);
        Mockito.when(utils.utilFunc1(Mockito.anyString())).thenReturn(false);

        MyBranching myBranching = new MyBranching(utils);
        myBranching.switchExample(i);

        Mockito.verify(utils, Mockito.times(1)).utilFunc2();
        Mockito.verify(utils, Mockito.times(1)).utilFunc1(Mockito.anyString());
    }
    /**
     * Проверка выполнения Utils#utilFunc1 = false и Utils#utilFunc2 = false при входящем i = 0
     */
    @Test
    public void switchExampleValue0Func2FalseTest() {
        int i = 0;
        Utils utils = Mockito.mock(Utils.class);
        Mockito.when(utils.utilFunc2()).thenReturn(false);
        Mockito.when(utils.utilFunc1(Mockito.anyString())).thenReturn(false);

        MyBranching myBranching = new MyBranching (utils);
        myBranching.switchExample(i);

        Mockito.verify(utils, Mockito.times(1)).utilFunc2();
        Mockito.verify(utils, Mockito.never()).utilFunc1("abc2");
    }
    /**
     * Проверка выполнения Utils#utilFunc1 = false и Utils#utilFunc2 = false при входящем i = 1
     */
    @Test
    public void switchExampleValue1Test() {
        int i = 1;
        Utils utils = Mockito.mock(Utils.class);
        Mockito.when(utils.utilFunc2()).thenReturn(false);
        Mockito.when(utils.utilFunc1(Mockito.anyString())).thenReturn(false);

        MyBranching myBranching = new MyBranching(utils);
        myBranching.switchExample(i);

        Mockito.verify(utils, Mockito.times(1)).utilFunc2();
        Mockito.verify(utils, Mockito.times(1)).utilFunc1("abc");
    }
    /**
     * Проверка выполнения Utils#utilFunc1 = false и Utils#utilFunc2 = false при входящем i = 2
     */
    @Test
    public void switchExampleValue2Test() {
        int i = 2;
        Utils utils = Mockito.mock(Utils.class);
        Mockito.when(utils.utilFunc2()).thenReturn(false);
        Mockito.when(utils.utilFunc1(Mockito.anyString())).thenReturn(false);

        MyBranching myBranching = new MyBranching(utils);
        myBranching.switchExample(i);

        Mockito.verify(utils, Mockito.times(1)).utilFunc2();
        Mockito.verify(utils, Mockito.never()).utilFunc1(Mockito.anyString());
    }
}

